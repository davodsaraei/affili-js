function decodeURI(t) {
  return decodeURIComponent(t.replace(/\+/g, ' '));
}

/**
* Return URL parameters as a array
*/
const getUrlVars = () => {
  let result = {},
    query = location.search.substring(1).split('&'),
    hash = location.hash.substring(1).split('&'),
    params = query.concat(hash);

  for (let i = 0; i < params.length; i++) {
    let paramArr = params[i].toString().split('=');
    if (paramArr.length > 1) {
      let key = decodeURI(paramArr[0]), value = decodeURI(paramArr[1]);
      result[key] = value;
    }
  }

  return result;
}

/**
* @param {string} parameter
* @param {string} defaultvalue
*/
const getUrlParam = (parameter, defaultvalue = null) => {
  return getUrlVars()[parameter] || defaultvalue;
}

/**
* The ajax method send a request
*
* @param {string} method
* @param {string} url
* @param {object} data
* @param {function} callback
*/
const ajax = (method, url, data) => {
  return new Promise((resolve, reject) => {
    let xhr = 'undefined' != typeof XMLHttpRequest ? new XMLHttpRequest : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.withCredentials = true;

    xhr.addEventListener('loadend', (event) => {
      if (event.target.status >= 200 && event.target.status < 400) {
        try {
          resolve( JSON.parse(event.target.response) );
        } catch(e) {
          resolve();
        }
      } else {
        reject(event);
      }
    })

    let json = JSON.stringify(data);

    xhr.open(method, url);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.setRequestHeader('Access-Control-Allow-Headers', 'x-requested-with');

    xhr.send(json);
  })
}

/**
 * The request object used to get URL parameters
 */
module.exports = {
  getUrlVars,
  getUrlParam,
  ajax,
}