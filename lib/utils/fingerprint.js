const afflVid = 'affl_vid'
const afflVidDate = 'affl_vid_date'

const init = () => {
  const vidDate = localStorage.getItem(afflVidDate)
  const currentTime = (new Date).getTime()
  const fingerVal = localStorage.getItem(afflVid) || ''

  if (currentTime - Number(vidDate) > 864e6 || !fingerVal.length) {
    new Promise(function (resolve) {
      const script = document.createElement('script')
      script.src = 'https://analytics.affili.ir/scripts/fingerprint.js'
      script.defer = true
      script.type = 'text/javascript'
      document.head.appendChild(script)
      script.onload = resolve
    }).then(function () {
      if (window.afflFinger !== undefined) {
        window.afflFinger.load().then(function (fpPromise) {
          return fpPromise.get()
        }).then(function (result) {
          localStorage.setItem(afflVid, result.visitorId)
          localStorage.setItem(afflVidDate, currentTime)
        })
      }
    })
  }
}

const get = () => {
  return localStorage.getItem(afflVid)
}

module.exports = {
  init,
  get
}