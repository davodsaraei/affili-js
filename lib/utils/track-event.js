const request = require('./request')

const trackEvent = (type, payload, debugMode = false) => {
  if (debugMode) {
    console.log({
      type: type,
      url: window.location.href,
      payload: payload,
      sourceCode: 'npm-package'
    });
  } else {
    request.ajax('POST', 'http://trk.affili.ir/events', {
      type: type,
      page: document.querySelector('html').innerHTML,
      url: window.location.href,
      payload: payload,
      sourceCode: 'npm-package'
    });
  }
}

module.exports = trackEvent