const request = require('../utils/request');
const cookies = require('js-cookie');
const trackEvent = require('../utils/track-event');
const fingerprint = require('../utils/fingerprint');


const conversionUrl = 'https://core.affili.ir/api/v2/clients/conversion';


const init = (callback = function () {}) => {
  fingerprint.init();

  const reqParams = request.getUrlVars();
  const vars = {
    referrer: reqParams['referrer'] || null,
    deleteCookie: reqParams['dl'] || 1,
    validDays: reqParams['exp'] || 30,
  }

  if (vars.referrer === null) {
    console.info('AFFILI -> ::create:: Nothing to detect.');

    return false;
  }

  cookies.set('AFFILI_DATA', btoa(JSON.stringify(vars)), {
    expires: parseInt(vars.validDays) || 30
  });

  callback();
  console.info('AFFILI -> ::create:: Cookies are set.');
}

// TODO: need to better approach
const crossDomain = (src, callback = function () {}) => {
  const reqParams = request.getUrlVars();
  const vars = {
    referrer: reqParams['referrer'] || null,
    deleteCookie: reqParams['dl'] || 1,
    validDays: reqParams['exp'] || 30,
  }

  if (vars.referrer === null) {
    return false;
  }

  setTimeout(() => {
    const ifrm = document.createElement('iframe')
    ifrm.setAttribute('id', 'aff-cross-domain-mode')
    ifrm.style.display = 'none'

    document.body.appendChild(ifrm)

    const url = new URL(src)

    url.searchParams.set('referrer', vars.referrer)
    url.searchParams.set('dl', vars.deleteCookie)
    url.searchParams.set('exp', vars.validDays)

    // assign url
    ifrm.setAttribute('src', url);

    callback();
  }, 1000);
}

const sale = (orderId, amount, options = {}, deleteCookie = true, callback = function () {}) => {
  const vars = JSON.parse(atob( cookies.get('AFFILI_DATA') || '' ) || '{}');

  if ( !vars.referrer ) {
    console.info('AFFILI -> ::conversion:: Cookies are not set.');
    return false;
  }

  const data = {
    meta_data: null,
    coupon: null,
    products: null,

    ...options,

    referrer: vars.referrer,
    order_id: orderId,
    amount: amount,
    type: 'sale',
    visitorId: fingerprint.get(),
  }

  // Save conversion and reset cookies as a default
  request.ajax('POST', conversionUrl, data).then(response => {
    // As a default we reset cookies
    if ( ['1', 1, 'true', true].includes(vars.deleteCookie) && deleteCookie === true ) {
      reset();
    }

    console.info('AFFILI -> ::conversion:: The conversion tracked, successfully.');
  }).catch(error => {
    // send data to the err tracker.
    trackEvent('req-ajax-error', {
      err: {
        message: error.target.response,
        status: error.target.status,
        paylod: {
          data: JSON.stringify(data),
          method: 'POST',
          reqUrl: conversionUrl
        }
      }
    });
  });

  callback();
}

const lead = (options = {}, callback = function () {}) => {
  const vars = JSON.parse(atob( cookies.get('AFFILI_DATA') || '' ) || '{}');

  if ( vars.referrer ) {
    console.info('AFFILI -> ::lead:: Cookies are not set.');
    return false;
  }

  const data = {
    meta_data: null,

    ...options,

    referrer: vars.referrer,
    type: 'lead',
    visitorId: fingerprint.get(),
  }

  request.ajax('POST', conversionUrl, data).then(response => {
    console.info('AFFILI -> ::lead:: The lead tracked, successfully.')
  }).catch(event => {
    // send data to the err tracker.
    trackEvent('req-ajax-error', {
      err: {
        message: error.target.response,
        status: error.target.status,
        paylod: {
          data: JSON.stringify(data),
          method: 'POST',
          reqUrl: conversionUrl
        }
      }
    });
  });

  callback();
}

const reset = (callback = function () {}) => {
  cookies.remove('AFFILI_DATA');

  callback();
}

const log = (callback = function () {}) => {
  const AFFILI_DATA = JSON.parse( atob( cookies.get('AFFILI_DATA') || '' ) || '{}' );

  console.table(AFFILI_DATA);

  callback();
}

const __data__ = (callback = function () {}) => {
  const data = JSON.parse( atob( cookies.get('AFFILI_DATA') || '' ) || '{}' );

  callback(data);
}

module.exports = {
  init,
  crossDomain,
  sale,
  lead,
  reset,
  log,
  __data__,
}