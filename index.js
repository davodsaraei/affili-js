"use strict";

const trackEvent = require('./lib/utils/track-event')
const methods = require('./lib/affili/v2')

module.exports = (methodName, ...params) => {
  if (typeof methods[methodName] !== 'undefined') {
    try {
      methods[methodName](...params)
    } catch (e) {
      trackEvent('execute-error', {
        err: {
          stack: e.stack || null,
          message: e.message || null
        }
      })
    }
  } else {
    console.warn('AFFILI -> ::execute:: The "' + methodName + '" function does not exists!')
  }
}